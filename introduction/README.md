### 版本说明
本示例配套的SDK版本为：3.1.2及以上版本

### 示例简介
本示例展示如何使用企业路由器路由表（RouteTable）相关SDK

### 功能介绍
企业路由器路由表的增、删、改、查功能。由于创建、更新和删除操作是异步接口，在进行调试时，需要注释无关的代码。
以创建企业路由器路由表为例, 进行测试时，将其它功能的测试函数注释掉。
```java
public static void main(String[] args) {
    String ak = "<your ak>";
    String sk = "<your sk>";
    String erId = "{er_id}";
    String routeTableId = "{route_table_id}";

    ICredential auth = new BasicCredentials()
    .withAk(ak)
    .withSk(sk);

    ErClient client = ErClient.newBuilder()
    .withCredential(auth)
    .withRegion(ErRegion.valueOf("cn-south-1"))
    .build();

    // Create route table
    createRouteTable(client, erId);

    // Update route table
    // updateRouteTable(client, erId, routeTableId);

    // Show route table
    // showRouteTable(client, erId, routeTableId);

    // Delete route table
    // deleteRouteTable(client, erId, routeTableId);

    // List route tables
    // listRouteTables(client, erId);
    }
```

### 前置条件
1.已 [注册](https://id1.cloud.huawei.com/UnifiedIDMPortal/portal/userRegister/regbyphone.html?themeName=red&access_type=offline&clientID=103493351&loginChannel=88000000&loginUrl=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2Flogin.html%23&service=https%3A%2F%2Fauth.huaweicloud.com%2Fauthui%2FcasLogin&countryCode=cn&scope=https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Funified.profile+https%3A%2F%2Fwww.huawei.com%2Fauth%2Faccount%2Frisk.idstate&reqClientType=88&state=cf496c0cb6c7414894276bebbfe1068c&lang=zh-cn) 华为云，并完成 [实名认证](https://account.huaweicloud.com/usercenter/?region=cn-north-4#/accountindex/realNameAuth)

2.获取华为云开发工具包（SDK），您也可以查看安装JAVA SDK。

3.已获取华为云账号对应的Access Key（AK）和Secret Access Key（SK）。请在华为云控制台“我的凭证 > 访问密钥”页面上创建和查看您的AK/SK。具体请参见 [访问密钥](https://support.huaweicloud.com/usermanual-ca/zh-cn_topic_0046606340.html) 。

4.已具备开发环境 ，支持Java JDK 1.8及其以上版本。

### SDK获取和安装
您可以通过Maven配置所依赖的企业路由器服务SDK

具体的SDK版本号请参见 [SDK开发中心](https://sdkcenter.developer.huaweicloud.com?language=java)  (产品类别：企业路由器)

### 代码示例
以下代码展示如何使用企业路由器路由表（RouteTable）相关SDK
``` java
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesRequest;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesResponse;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterRouteTableDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterRouteTableDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<your ak>";
        String sk = "<your sk>";
        String erId = "{er_id}";
        String routeTableId = "{route_table_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

        // Create route table
        CreateRouteTableResponse response = createRouteTable(client, erId);

        // Update route table
        updateRouteTable(client, erId, routeTableId);

        // Show route table
        showRouteTable(client, erId, routeTableId);

        // Delete route table
        deleteRouteTable(client, erId, routeTableId);

        // List route tables
        listRouteTables(client, erId);
    }

    private static CreateRouteTableResponse createRouteTable(ErClient client, String erId) {
        CreateRouteTableRequest request = new CreateRouteTableRequest();
        CreateRouteTableRequestBody body = new CreateRouteTableRequestBody();
        body.withRouteTable(routeTable -> {
            routeTable.setName("<your route table name>");
            routeTable.setDescription("<your route table description>");
        });
        request.withErId(erId).withBody(body);
        Function<Void, CreateRouteTableResponse> task = (Void v) -> {
            return client.createRouteTable(request);
        };
        return execute(task);
    }

    private static UpdateRouteTableResponse updateRouteTable(ErClient client, String erId, String routeTableId) {
        UpdateRouteTableRequest request = new UpdateRouteTableRequest();
        UpdateRouteTableRequestBody body = new UpdateRouteTableRequestBody();
        body.withRouteTable(routeTable -> {
            routeTable.setName("<your new route table name>");
            routeTable.setDescription("<your new route table description>");
        });
        request.withErId(erId).withRouteTableId(routeTableId).withBody(body);
        Function<Void, UpdateRouteTableResponse> task = (Void v) -> {
            return client.updateRouteTable(request);
        };
        return execute(task);
    }

    private static ShowRouteTableResponse showRouteTable(ErClient client, String erId, String routeTableId) {
        ShowRouteTableRequest request = new ShowRouteTableRequest();
        request.withErId(erId).withRouteTableId(routeTableId);
        Function<Void, ShowRouteTableResponse> task = (Void v) -> {
            return client.showRouteTable(request);
        };
        return execute(task);
    }

    private static DeleteRouteTableResponse deleteRouteTable(ErClient client, String erId, String routeTableId) {
        DeleteRouteTableRequest request = new DeleteRouteTableRequest();
        request.withErId(erId).withRouteTableId(routeTableId);
        Function<Void, DeleteRouteTableResponse> task = (Void v) -> {
            return client.deleteRouteTable(request);
        };
        return execute(task);
    }

    private static ListRouteTablesResponse listRouteTables(ErClient client, String erId) {
        ListRouteTablesRequest request = new ListRouteTablesRequest();
        request.withErId(erId);
        Function<Void, ListRouteTablesResponse> task = (Void v) -> {
            return client.listRouteTables(request);
        };
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
```
您可以在 [企业路由器服务文档](https://support.huaweicloud.com/productdesc-er/er_01_0002.html) 和[API Explorer](https://apiexplorer.developer.huaweicloud.com/apiexplorer/doc?product=ER&api=CreateRouteTable) 查看具体信息。

### 修订记录

发布日期  | 文档版本 | 修订说明
 :----: | :-----: | :------:  
2022/09/27 |1.0 | 文档首次发布
