package com.huawei.demo;

import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.exception.ClientRequestException;
import com.huaweicloud.sdk.core.exception.ServerResponseException;
import com.huaweicloud.sdk.er.v3.ErClient;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.CreateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.DeleteRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesRequest;
import com.huaweicloud.sdk.er.v3.model.ListRouteTablesResponse;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.ShowRouteTableResponse;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequest;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableRequestBody;
import com.huaweicloud.sdk.er.v3.model.UpdateRouteTableResponse;
import com.huaweicloud.sdk.er.v3.region.ErRegion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

public class EnterpriseRouterRouteTableDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnterpriseRouterRouteTableDemo.class.getName());

    public static void main(String[] args) {
        String ak = "<your ak>";
        String sk = "<your sk>";
        String erId = "{er_id}";
        String routeTableId = "{route_table_id}";

        ICredential auth = new BasicCredentials()
            .withAk(ak)
            .withSk(sk);

        ErClient client = ErClient.newBuilder()
            .withCredential(auth)
            .withRegion(ErRegion.valueOf("cn-south-1"))
            .build();

        // Create route table
        createRouteTable(client, erId);

        // Update route table
        updateRouteTable(client, erId, routeTableId);

        // Show route table
        showRouteTable(client, erId, routeTableId);

        // Delete route table
        deleteRouteTable(client, erId, routeTableId);

        // List route tables
        listRouteTables(client, erId);
    }

    private static CreateRouteTableResponse createRouteTable(ErClient client, String erId) {
        CreateRouteTableRequest request = new CreateRouteTableRequest();
        CreateRouteTableRequestBody body = new CreateRouteTableRequestBody();
        body.withRouteTable(routeTable -> {
            routeTable.setName("<your route table name>");
            routeTable.setDescription("<your route table description>");
        });
        request.withErId(erId).withBody(body);
        Function<Void, CreateRouteTableResponse> task = (Void v) -> {
            return client.createRouteTable(request);
        };
        return execute(task);
    }

    private static UpdateRouteTableResponse updateRouteTable(ErClient client, String erId, String routeTableId) {
        UpdateRouteTableRequest request = new UpdateRouteTableRequest();
        UpdateRouteTableRequestBody body = new UpdateRouteTableRequestBody();
        body.withRouteTable(routeTable -> {
            routeTable.setName("<your new route table name>");
            routeTable.setDescription("<your new route table description>");
        });
        request.withErId(erId).withRouteTableId(routeTableId).withBody(body);
        Function<Void, UpdateRouteTableResponse> task = (Void v) -> {
            return client.updateRouteTable(request);
        };
        return execute(task);
    }

    private static ShowRouteTableResponse showRouteTable(ErClient client, String erId, String routeTableId) {
        ShowRouteTableRequest request = new ShowRouteTableRequest();
        request.withErId(erId).withRouteTableId(routeTableId);
        Function<Void, ShowRouteTableResponse> task = (Void v) -> {
            return client.showRouteTable(request);
        };
        return execute(task);
    }

    private static DeleteRouteTableResponse deleteRouteTable(ErClient client, String erId, String routeTableId) {
        DeleteRouteTableRequest request = new DeleteRouteTableRequest();
        request.withErId(erId).withRouteTableId(routeTableId);
        Function<Void, DeleteRouteTableResponse> task = (Void v) -> {
            return client.deleteRouteTable(request);
        };
        return execute(task);
    }

    private static ListRouteTablesResponse listRouteTables(ErClient client, String erId) {
        ListRouteTablesRequest request = new ListRouteTablesRequest();
        request.withErId(erId);
        Function<Void, ListRouteTablesResponse> task = (Void v) -> {
            return client.listRouteTables(request);
        };
        return execute(task);
    }

    private static <T> T execute(Function<Void, T> task) {
        T response = null;
        try {
            response = task.apply(null);
            LOGGER.info(response.toString());
        } catch (ClientRequestException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.toString());
        } catch (ServerResponseException e) {
            LOGGER.error(String.valueOf(e.getHttpStatusCode()));
            LOGGER.error(e.getMessage());
        }
        return response;
    }
}
