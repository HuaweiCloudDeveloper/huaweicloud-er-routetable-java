#!/bin/bash

PROJECT_ROOT=$(cd `dirname $0/`/..;pwd)
cd $PROJECT_ROOT/code
mvn -e clean -U -Dmaven.test.skip package

cd $PROJECT_ROOT
tar -czvf $PROJECT_ROOT/huaweicloud-er-routetable-java-${CID_BUILD_TIME}.tar.gz .codelabs code introduction
